Install JAVA jdk 1.8.0_60
Install MVN 3

1. mvn eclipse:eclipse clean install
2. You can import the directory as a new Eclipse project if you want
3. mvn spring-boot:run
4. Go to: http://localhost:8081 (database is at: http://localhost:8081/console  ////// url: jdbc:h2:mem:testdb)