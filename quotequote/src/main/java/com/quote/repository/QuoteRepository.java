package com.quote.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.quote.domain.QuoteDomain;
import com.quote.dto.request.QuoteByOwnerRequest;
import com.quote.dto.request.QuoteByTagRequest;
import com.quote.mapper.QuoteMapper;

@Repository
public class QuoteRepository implements IQuoteRepository{

	private static final String PAGE = " "
			+ "LIMIT :limit"
			+ " "
			+ "OFFSET :offset";
	
	private static final String ORDER_BY = "ORDER BY :sortedColumn asc";
	
	private static final String SELECT_ALL = "SELECT"
			+ " "
			+ "quote_id"
			+ ",owner_user_id"
			+ ",name"
			+ ",s_tags"
			+ ",quote_text"
			+ ",description"
			+ ",url"
			+ ",attributed_to"
			+ ",image"
			+ ",layout"
			+ ",sound"
			+ ",interest"
			+ ",creation_date"
			+ ",last_modified_date"
			+ ",last_viewed_date"
			+ ",times_viewed"
			+ ",mood"
			+ ",copied_from"
			+ ",is_private"
			+ " "
			+ "FROM"
			+ " "
			+ "QUOTE"
			+ " ";

	private static final String SELECT_COUNT = "SELECT "
			+ " "
			+ "count(*)"
			+ " "
			+ "FROM"
			+ " "
			+ "QUOTE"
			+ " ";

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	public int createQuote(QuoteDomain quote) {

		String query = "INSERT "
				+ "into "
				+ "QUOTE"
				+ "("
				+ "owner_user_id"
				+ ",name"
				+ ",s_tags"
				+ ",quote_text"
				+ ",description"
				+ ",url"
				+ ",attributed_to"
				+ ",interest"
				+ ",creation_date"
				+ ",mood"
				+ ",is_private"
				+ ")"
				+ " values "
				+ "("
				+ ":ownerUserId"
				+ ",:name"
				+ ",:sTags"
				+ ",:quoteText"
				+ ",:description"
				+ ",:url"
				+ ",:attributedTo"
				+ ",:interest"
				+ ",:creationDate"
				+ ",:mood"
				+ ",:isPrivate"
				+ ")";


		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(quote);
		return jdbcTemplate.update(query, namedParameters);
	}

	@Override	
	public List<QuoteDomain> getQuotesByOwner(QuoteByOwnerRequest request) {

		String query = SELECT_ALL 
				+ "WHERE"
				+ " "
				+ "owner_user_id = :owner_user_id"
				+ " "
				+ "order by "+request.getSortingInfo().getSortedColumn()
				+ " "
				+ request.getSortingInfo().getSortOrder().toString()
				+ " "
				+ "limit"
				+ " " +request.getPaginationInfo().getPageSize()
				+" "
				+ "offset"
				+ " "
				+ request.getPaginationInfo().getOffset();

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

	    namedParameters.addValue("owner_user_id", request.getOwnerId());
		
		return jdbcTemplate.query(query, namedParameters, new QuoteMapper());

	}

	@Override
	public int getQuoteCountByOwner(Long ownerId) {

		String query = SELECT_COUNT + "WHERE owner_user_id = :owner_user_id";

		SqlParameterSource namedParameters = new MapSqlParameterSource("owner_user_id", ownerId);
		return jdbcTemplate.queryForObject(query, namedParameters, Integer.class);

	}

	@Override
	public int getQuoteCountByTag(Long ownerId,  String tag) {

		String query = SELECT_COUNT + "WHERE owner_user_id = :owner_user_id and s_tags like :s_tags";
	    
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

	    namedParameters.addValue("s_tags", "%"+tag+"%");
	    namedParameters.addValue("owner_user_id", ownerId);

		return jdbcTemplate.queryForObject(query, namedParameters, Integer.class);
	}

	@Override
	public QuoteDomain getQuoteById(Long id) {
		
		String query = SELECT_ALL + "WHERE quote_id = :quote_id";

		MapSqlParameterSource namedParameters = new MapSqlParameterSource("quote_id", id);

		return jdbcTemplate.queryForObject(query, namedParameters, new QuoteMapper());
		
	}
	
	

	
	@Override
	public List<QuoteDomain> getQuotesByTag(QuoteByTagRequest request) throws DataAccessException {
		
		String query = SELECT_ALL 
				+ "WHERE owner_user_id = :owner_user_id "
				+ "and s_tags like :s_tags"
				+ " "
				+ "order by "+request.getSortingInfo().getSortedColumn()
				+ " "
				+ request.getSortingInfo().getSortOrder().toString()
				+ " "
				+ "limit"
				+ " " +request.getPaginationInfo().getPageSize()
				+" "
				+ "offset"
				+ " "
				+ request.getPaginationInfo().getOffset();
	    
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

	    namedParameters.addValue("s_tags", "%"+request.getTag()+"%");
	    namedParameters.addValue("owner_user_id", request.getOwnerId());

	    return jdbcTemplate.query(query, namedParameters, new QuoteMapper());

	}

	@Override
	public int updateQuoteVisitedCount(Long id, int count, Long lastVisitedDate) {
		
		String query = "update QUOTE"
				+ " "
				+ "set times_viewed=:count"
				+ " "
				+ "where"
				+ " "
				+ "quote_id=:quote_id"
				+ " "
				+ "and"
				+ " "
				+ "last_viewed_date=:last_viewed_date";
		
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
	    namedParameters.addValue("count", count);
	    namedParameters.addValue("quote_id", id);
	    namedParameters.addValue("last_viewed_date", lastVisitedDate);

	    return jdbcTemplate.update(query, namedParameters);

	}

}
