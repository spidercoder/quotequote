package com.quote.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.quote.domain.QuoteDomain;
import com.quote.dto.request.QuoteByOwnerRequest;
import com.quote.dto.request.QuoteByTagRequest;

public interface IQuoteRepository {
	int createQuote(QuoteDomain request);
	
	List<QuoteDomain> getQuotesByOwner(QuoteByOwnerRequest request) throws DataAccessException;
	
	List<QuoteDomain> getQuotesByTag(QuoteByTagRequest request) throws DataAccessException;
	
	int getQuoteCountByOwner(Long ownerId);
	
	int getQuoteCountByTag(Long ownerId, String tag);
	
	QuoteDomain getQuoteById(Long id);
	
	int updateQuoteVisitedCount(Long id, int count, Long lastVisitedDate);
}
