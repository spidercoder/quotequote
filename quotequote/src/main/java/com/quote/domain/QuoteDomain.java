package com.quote.domain;


public class QuoteDomain {
	
	/*quote_id                  bigserial not null,
	  owner_user_id             bigint default 0,          -- The use this quote belongs to
	  name                      varchar(255) default '',  -- Simple identification for this quote. Like a recipe name.
	  s_tags                    varchar(6000) default '',  -- A hashtag deliminated list of tags, such as '#inspirational #famous'
	  quote_text								varchar(6000) default '',  -- The text to save and display
	  description               varchar(6000) default '',  -- User specific comment
	  url                       varchar(255) default '',  -- URL the quote came from.
		attributed_to             varchar(1000) default '',  -- More information on where this quote came from
		image                     varchar(1000) default null,  -- What image should backdrop this quote
		layout                    varchar(1000) default null,  -- Where the text should be display. Top? Bottom? Left? Right?
		sound                     varchar(1000) default null,  -- What sound should play when this text is shown.
	  interest                  integer default 5,  -- How interested the user is in this quote.
	  creation_date             bigint default 0,  -- When this quote was created.
	  last_modified_date        bigint default 0,  -- When this quote was last modified
	  last_viewed_date          bigint default 0,  -- When this quote was last viewed
	  times_viewed              integer default 0,  -- Number of times the quote was viewed
		mood                      varchar(6000) default null,  -- The user's mood when they last viewed this quote
		copied_from               bigint default 0,  -- The global_quote object this quote was first copied from
	  is_private                boolean default false,*/
	  
	private Long quoteId;
	private Long ownerUserId;
	private String name;
	private String sTags;
	private String quoteText;
	private String description;
	private String url;
	private String attributedTo;
	private String image;
	private String layout;
	private String sound;
	private int interest;
	private Long creationDate;
	private Long lastModifiedDate;
	private Long lastViewedDate;
	private int timesViewed;
	private String mood;
	private Long copiedFrom;
	private boolean isPrivate;
	
	
	public Long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}
	public Long getOwnerUserId() {
		return ownerUserId;
	}
	public void setOwnerUserId(Long ownerUserId) {
		this.ownerUserId = ownerUserId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getsTags() {
		return sTags;
	}
	public void setsTags(String sTags) {
		this.sTags = sTags;
	}
	public String getQuoteText() {
		return quoteText;
	}
	public void setQuoteText(String quoteText) {
		this.quoteText = quoteText;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAttributedTo() {
		return attributedTo;
	}
	public void setAttributedTo(String attributedTo) {
		this.attributedTo = attributedTo;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public String getSound() {
		return sound;
	}
	public void setSound(String sound) {
		this.sound = sound;
	}
	public int getInterest() {
		return interest;
	}
	public void setInterest(int interest) {
		this.interest = interest;
	}
	public Long getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Long creationDate) {
		this.creationDate = creationDate;
	}
	public Long getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Long lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public Long getLastViewedDate() {
		return lastViewedDate;
	}
	public void setLastViewedDate(Long lastViewedDate) {
		this.lastViewedDate = lastViewedDate;
	}
	public int getTimesViewed() {
		return timesViewed;
	}
	public void setTimesViewed(int timesViewed) {
		this.timesViewed = timesViewed;
	}
	public String getMood() {
		return mood;
	}
	public void setMood(String mood) {
		this.mood = mood;
	}
	public Long getCopiedFrom() {
		return copiedFrom;
	}
	public void setCopiedFrom(Long copiedFrom) {
		this.copiedFrom = copiedFrom;
	}
	public boolean getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((quoteId == null) ? 0 : quoteId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuoteDomain other = (QuoteDomain) obj;
		if (quoteId == null) {
			if (other.quoteId != null)
				return false;
		} else if (!quoteId.equals(other.quoteId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Quote [quoteId=" + quoteId + ", ownerUserId=" + ownerUserId + ", name=" + name + ", sTags=" + sTags
				+ ", quoteText=" + quoteText + ", description=" + description + ", url=" + url + ", attributedTo="
				+ attributedTo + ", image=" + image + ", layout=" + layout + ", sound=" + sound + ", interest="
				+ interest + ", creationDate=" + creationDate + ", lastModifiedDate=" + lastModifiedDate
				+ ", lastViewedDate=" + lastViewedDate + ", timesViewed=" + timesViewed + ", mood=" + mood
				+ ", copiedFrom=" + copiedFrom + ", isPrivate=" + isPrivate + "]";
	}
	
	
	
	
}
