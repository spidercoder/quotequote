package com.quote.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.quote.domain.QuoteDomain;

public class QuoteMapper implements RowMapper<QuoteDomain>{

	@Override
	public QuoteDomain mapRow(ResultSet rs, int arg1) throws SQLException {
		
		QuoteDomain quote = new QuoteDomain();
		quote.setQuoteId(rs.getLong("quote_id"));
		quote.setOwnerUserId(rs.getLong("owner_user_id"));
		quote.setName(rs.getString("name"));
		quote.setsTags(rs.getString("s_tags"));
		quote.setQuoteText(rs.getString("quote_text"));
		quote.setDescription(rs.getString("description"));
		quote.setUrl(rs.getString("url"));
		quote.setAttributedTo(rs.getString("attributed_to"));
		quote.setImage(rs.getString("image"));
		quote.setLayout(rs.getString("layout"));
		quote.setSound(rs.getString("sound"));
		quote.setInterest(rs.getInt("interest"));
		quote.setCreationDate(rs.getLong("creation_date"));
		quote.setLastModifiedDate(rs.getLong("last_modified_date"));
		quote.setLastViewedDate(rs.getLong("last_viewed_date"));
		quote.setTimesViewed(rs.getInt("times_viewed"));
		quote.setMood(rs.getString("mood"));
		quote.setCopiedFrom(rs.getLong("copied_from"));
		quote.setIsPrivate(rs.getBoolean("is_private"));
		return quote;
	}

}
