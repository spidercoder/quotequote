package com.quote.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.quote.dto.Quote;
import com.quote.dto.SortOrder;
import com.quote.dto.request.PaginationInfo;
import com.quote.dto.request.QuoteByOwnerRequest;
import com.quote.dto.request.QuoteByTagRequest;
import com.quote.dto.request.QuoteRequest;
import com.quote.dto.request.SortingInfo;
import com.quote.dto.response.QuoteResponse;
import com.quote.service.IQuoteService;

@RequestMapping("/v1/quotes")
@Controller
public class QuoteController {
	
	@Autowired
	IQuoteService quoteService;
	
	@RequestMapping(value = "/createQuote", method = RequestMethod.POST)
	public @ResponseBody HttpStatus createQuote(@RequestBody QuoteRequest request) {
		
		System.out.println(request.getName());
		Long ownerId = 1l;
		int rowsAffected = quoteService.addQuote(request, ownerId);
		
		if(rowsAffected == 0) {
			return HttpStatus.BAD_REQUEST;
		}
		
		return HttpStatus.ACCEPTED;
	}
	
	/*@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody QuoteResponse getPersonalQuotes() {

		Long ownerId = 1l;
		QuoteResponse response = quoteService.getQuotesByOwner(ownerId);
		return response;
	}*/

	@RequestMapping(value = "/{quoteId}",method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Quote getQuoteById(@PathVariable Long quoteId) {
		
		return quoteService.getQuoteById(quoteId);
		
	}
	
	/*@RequestMapping(value = "/tags/{tag}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody QuoteResponse getQuotesByTag(@PathVariable String tag) {
		return quoteService.getQuotesByTag(1l, tag);
	}*/
	
	/*
	 * dashboard: 
	 * 	personal: 
	 * 		owner id, 
	 * 		sortby -> most-viewed(default)
	 * 		offset ->0
	 * 		limit -> 10
	 * 
	 * 	recommendations (dont worry for now)
	 * 
	 * Manage: personal
	 * 		owner id,
	 * 		sort-by -> most view(default), attributedTo, creation date, last visited
	 *      offset -> 0
	 *      limit -> 50
	 *      searchby -> quotetext(default), tags - {single, extend to multiple}.
	 * 
	 * View quote - personal:  
	 * 		get quoteId
	 * 		next -> quoteId (front end, server call to update visits?)
	 * 		previous -> (front end, server call to update visits)
	 * */
	
	//contextByOwner
	@RequestMapping(value = "/order-by={orderBy}/sorted-as={sortedAs}/limit={limit}/offset={offset}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody QuoteResponse getNextQuotes(
			@PathVariable String orderBy,
			@PathVariable String sortedAs,
			@PathVariable int limit,
			@PathVariable int offset) {
		
		Long ownerId = 1l;
		SortingInfo sortingInfo = new SortingInfo(orderBy);
		sortingInfo.setSortOrder(SortOrder.valueOf(sortedAs));
		PaginationInfo paginationInfo = new PaginationInfo(offset);
		paginationInfo.setPageSize(limit);
		
		QuoteByOwnerRequest request = new QuoteByOwnerRequest(sortingInfo, paginationInfo, ownerId);
		
		
		return quoteService.getQuotesByOwner(request);
	}
	
	
	@RequestMapping(value = "/tags/{tag}/order-by={orderBy}/sorted-as={sortedAs}/limit={limit}/offset={offset}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody QuoteResponse getNextQuoteByTag(
			@PathVariable String tag,
			@PathVariable String orderBy,
			@PathVariable String sortedAs,
			@PathVariable int limit,
			@PathVariable int offset) {
		
		Long ownerId = 1l;
		SortingInfo sortingInfo = new SortingInfo(orderBy);
		sortingInfo.setSortOrder(SortOrder.valueOf(sortedAs));
		PaginationInfo paginationInfo = new PaginationInfo(offset);
		paginationInfo.setPageSize(limit);
		
		QuoteByTagRequest request = new QuoteByTagRequest(sortingInfo, paginationInfo, ownerId, tag);
		
		return quoteService.getQuotesByTag(request);
	}
	
	
}
