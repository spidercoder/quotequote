package com.quote.dto;

public class Quote {
	private Long quoteId;
	private Long ownerUserId;
	private String name;
	private String sTags;
	private String quoteText;
	private String description;
	private String url;
	private String attributedTo;
	private int interest;
	private String creationDate;
	private String lastModifiedDate;
	private String lastViewedDate;
	private int timesViewed;
	private String mood;
	public Long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}
	public Long getOwnerUserId() {
		return ownerUserId;
	}
	public void setOwnerUserId(Long ownerUserId) {
		this.ownerUserId = ownerUserId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getsTags() {
		return sTags;
	}
	public void setsTags(String sTags) {
		this.sTags = sTags;
	}
	public String getQuoteText() {
		return quoteText;
	}
	public void setQuoteText(String quoteText) {
		this.quoteText = quoteText;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAttributedTo() {
		return attributedTo;
	}
	public void setAttributedTo(String attributedTo) {
		this.attributedTo = attributedTo;
	}
	public int getInterest() {
		return interest;
	}
	public void setInterest(int interest) {
		this.interest = interest;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getLastViewedDate() {
		return lastViewedDate;
	}
	public void setLastViewedDate(String lastViewedDate) {
		this.lastViewedDate = lastViewedDate;
	}
	public int getTimesViewed() {
		return timesViewed;
	}
	public void setTimesViewed(int timesViewed) {
		this.timesViewed = timesViewed;
	}
	public String getMood() {
		return mood;
	}
	public void setMood(String mood) {
		this.mood = mood;
	}

}
