package com.quote.dto;

public enum SortOrder {
	ASC, DESC
}
