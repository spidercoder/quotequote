package com.quote.dto.request;

public class QuoteByOwnerRequest {
	
	private final SortingInfo sortingInfo;
	private final PaginationInfo paginationInfo;
	
	private final Long ownerId;
	
	public QuoteByOwnerRequest(SortingInfo sortingInfo, PaginationInfo paginationInfo, Long ownerId) {
		this.sortingInfo = sortingInfo;
		this.paginationInfo = paginationInfo;
		this.ownerId = ownerId;
	}

	public SortingInfo getSortingInfo() {
		return sortingInfo;
	}

	public PaginationInfo getPaginationInfo() {
		return paginationInfo;
	}

	public Long getOwnerId() {
		return ownerId;
	}
	

}
