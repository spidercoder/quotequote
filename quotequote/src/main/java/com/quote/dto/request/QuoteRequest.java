package com.quote.dto.request;

public class QuoteRequest {
	
	private String quoteText;
	private String sTags;
	private String url;
	private String attributedTo;
	private int interest;
	private String name;
	private String mood;
	private String description;
	private boolean isPrivate;
	private String image;
	private long creationDate;
	private long lastModifiedDate;
	private long lastViewDate;
	
	public String getQuoteText() {
		return quoteText;
	}
	public void setQuoteText(String quoteText) {
		this.quoteText = quoteText;
	}
	public String getsTags() {
		return sTags;
	}
	public void setsTags(String sTags) {
		this.sTags = sTags;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAttributedTo() {
		return attributedTo;
	}
	public void setAttributedTo(String attributedTo) {
		this.attributedTo = attributedTo;
	}
	public int getInterest() {
		return interest;
	}
	public void setInterest(int interest) {
		this.interest = interest;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMood() {
		return mood;
	}
	public void setMood(String mood) {
		this.mood = mood;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isPrivate() {
		return isPrivate;
	}
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public long getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}
	public long getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(long lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public long getLastViewDate() {
		return lastViewDate;
	}
	public void setLastViewDate(long lastViewDate) {
		this.lastViewDate = lastViewDate;
	}
	
	
}
