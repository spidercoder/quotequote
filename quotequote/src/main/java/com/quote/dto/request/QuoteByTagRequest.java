package com.quote.dto.request;

public class QuoteByTagRequest {
	
	private final SortingInfo sortingInfo;
	private final PaginationInfo paginationInfo;
	
	private final Long ownerId;
	private final String tag;
	
	public QuoteByTagRequest(SortingInfo sortingInfo, PaginationInfo paginationInfo, Long ownerId, String tag) {
		this.sortingInfo = sortingInfo;
		this.paginationInfo = paginationInfo;
		this.ownerId = ownerId;
		this.tag = tag;
	}

	public SortingInfo getSortingInfo() {
		return sortingInfo;
	}

	public PaginationInfo getPaginationInfo() {
		return paginationInfo;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public String getTag() {
		return tag;
	}
	
}
