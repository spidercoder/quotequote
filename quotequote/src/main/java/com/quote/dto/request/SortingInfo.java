package com.quote.dto.request;

import com.quote.dto.SortOrder;

public class SortingInfo {
	private final String sortedColumn;

	private SortOrder sortOrder;
	
	public SortingInfo(String sortedColumn) {
		this.sortedColumn = sortedColumn;
		this.sortOrder = SortOrder.DESC;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public String getSortedColumn() {
		return sortedColumn;
	}
	
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	
	
}
