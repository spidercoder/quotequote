package com.quote.dto.request;

public class PaginationInfo {
	private final int offset;
	private int pageSize;
	
	public PaginationInfo(int offset) {
		this.offset = offset;
		pageSize = 10;
	}

	public int getOffset() {
		return offset;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
}
