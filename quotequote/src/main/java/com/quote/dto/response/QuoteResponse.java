package com.quote.dto.response;

import java.util.List;

import com.quote.dto.Quote;

public class QuoteResponse {
	
	List<Quote> quotes;
	Integer count;
	
	public QuoteResponse() {
		count = 0;
	}
	public List<Quote> getQuotes() {
		return quotes;
	}
	public void setQuotes(List<Quote> quotes) {
		this.quotes = quotes;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
}
