package com.quote.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quote.domain.QuoteDomain;
import com.quote.dto.Quote;
import com.quote.dto.request.QuoteByOwnerRequest;
import com.quote.dto.request.QuoteByTagRequest;
import com.quote.dto.request.QuoteRequest;
import com.quote.dto.response.QuoteResponse;
import com.quote.repository.IQuoteRepository;

@Service
public class QuoteService implements IQuoteService{

	/*quote_id                  bigserial not null,
	  owner_user_id             bigint default 0,          -- The use this quote belongs to
	  name                      varchar(255) default '',  -- Simple identification for this quote. Like a recipe name.
	  s_tags                    varchar(6000) default '',  -- A hashtag deliminated list of tags, such as '#inspirational #famous'
	  quote_text								varchar(6000) default '',  -- The text to save and display
	  description               varchar(6000) default '',  -- User specific comment
	  url                       varchar(255) default '',  -- URL the quote came from.
		attributed_to             varchar(1000) default '',  -- More information on where this quote came from
		image                     varchar(1000) default null,  -- What image should backdrop this quote
		layout                    varchar(1000) default null,  -- Where the text should be display. Top? Bottom? Left? Right?
		sound                     varchar(1000) default null,  -- What sound should play when this text is shown.
	  interest                  integer default 5,  -- How interested the user is in this quote.
	  creation_date             bigint default 0,  -- When this quote was created.
	  last_modified_date        bigint default 0,  -- When this quote was last modified
	  last_viewed_date          bigint default 0,  -- When this quote was last viewed
	  times_viewed              integer default 0,  -- Number of times the quote was viewed
		mood                      varchar(6000) default null,  -- The user's mood when they last viewed this quote
		copied_from               bigint default 0,  -- The global_quote object this quote was first copied from
	  is_private                boolean default false,*/
	
	private static final String DATE_FORMAT = "EEE, d MMM yyyy HH:mm z";
	@Autowired
	IQuoteRepository quoteRespository;
	
	@Autowired
	IDateUtils dateUtils;
	
	@Override
	public int  addQuote(QuoteRequest request, Long ownerId) {
		
		QuoteDomain quote = new QuoteDomain();
		
		quote.setOwnerUserId(ownerId);
		quote.setName(request.getName());
		quote.setsTags(request.getsTags());
		quote.setQuoteText(request.getQuoteText());
		quote.setDescription(request.getDescription());
		quote.setUrl(request.getUrl());
		quote.setAttributedTo(request.getAttributedTo());
		
		setInterest(quote, request.getInterest());
		
		quote.setMood(request.getMood());
		quote.setIsPrivate(request.isPrivate());
		quote.setCreationDate(request.getCreationDate());
		quote.setLastModifiedDate(request.getLastModifiedDate());
		quote.setLastViewedDate(request.getLastViewDate());
		quote.setImage(request.getImage());
		
		return quoteRespository.createQuote(quote);
		
	}

	private void setInterest(QuoteDomain quote, int interest) {
		if(interest > 0) {
			quote.setInterest(interest);
		}
	}
	
	@Override
	public QuoteResponse getQuotesByOwner(QuoteByOwnerRequest request) {
		
		QuoteResponse response = new QuoteResponse();
		List<Quote> quotes = new ArrayList<>();
		
		mapDomainToDTO(quotes, quoteRespository.getQuotesByOwner(request));
		
		response.setQuotes(quotes);
		if(request.getPaginationInfo().getOffset() == 0) {
			response.setCount(quoteRespository.getQuoteCountByOwner(request.getOwnerId()));
		}

		return response;
	}

	@Override
	public QuoteResponse getQuotesByTag(QuoteByTagRequest request) {
		QuoteResponse response = new QuoteResponse();
		
		List<Quote> quotes = new ArrayList<>();
		
		mapDomainToDTO(quotes, quoteRespository.getQuotesByTag(request));
		
		response.setQuotes(quotes);
		
		if(request.getPaginationInfo().getOffset() == 0) {
			response.setCount(quoteRespository.getQuoteCountByTag(request.getOwnerId(), request.getTag()));
		}
		
		return response;
	}
	
	@Override
	public int getQuoteCountByOwner(Long ownerId) {
		return quoteRespository.getQuoteCountByOwner(ownerId);
	}

	@Override
	public int getQuoteCountByTag(Long ownerId, String tag) {
		
		return quoteRespository.getQuoteCountByTag(ownerId, tag);
	}

	@Override
	public int updateQuote(QuoteRequest request) {
		
		return 0;
		
	}
	
	private void mapDomainToDTO(List<Quote> quotes, List<QuoteDomain> quotesFromDB) {
		
		for(QuoteDomain domainObj : quotesFromDB) {
		
			Quote quote = new Quote();
			mapDomainToDTO(quote, domainObj);
			quotes.add(quote);
		}
	}

	@Override
	public Quote getQuoteById(Long id) {
		
		Quote quote = new Quote();
		
		mapDomainToDTO(quote, quoteRespository.getQuoteById(id));
	
		quote.setTimesViewed(quote.getTimesViewed() + 1);
		Long date = System.currentTimeMillis();
		quote.setLastViewedDate(dateUtils.convertDateToString(date, DATE_FORMAT));
		
		quoteRespository.updateQuoteVisitedCount(id, quote.getTimesViewed(), date);
		return quote;
		
	}
	
	private void mapDomainToDTO(Quote quote, QuoteDomain domainObj) {
		quote.setQuoteId(domainObj.getQuoteId());
		quote.setOwnerUserId(domainObj.getOwnerUserId());
		quote.setName(domainObj.getName());
		quote.setsTags(domainObj.getsTags());
		quote.setQuoteText(domainObj.getQuoteText());
		quote.setDescription(domainObj.getDescription());
		quote.setUrl(domainObj.getUrl());
		quote.setAttributedTo(domainObj.getAttributedTo());
		quote.setInterest(domainObj.getInterest());
		quote.setCreationDate(dateUtils.convertDateToString(domainObj.getCreationDate(), DATE_FORMAT));
		quote.setLastModifiedDate(dateUtils.convertDateToString(domainObj.getLastModifiedDate(), DATE_FORMAT));
		quote.setLastViewedDate(dateUtils.convertDateToString(domainObj.getLastViewedDate(), DATE_FORMAT));
		quote.setTimesViewed(domainObj.getTimesViewed());
		quote.setMood(domainObj.getMood());
	}

	
}
