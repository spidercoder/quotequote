package com.quote.service;

import java.util.Date;

public interface IDateUtils {
	String convertDateToString(Long date, String format);
	Long convertStringToDate(String date, String format);
}
