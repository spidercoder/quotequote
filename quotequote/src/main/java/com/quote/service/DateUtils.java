package com.quote.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

@Service
public class DateUtils implements IDateUtils {

	private ThreadLocal<SimpleDateFormat> tl = new ThreadLocal<SimpleDateFormat>();

	private SimpleDateFormat getFormatter(String format) {
		SimpleDateFormat sdf = tl.get();   	
		if(sdf == null) {
		    sdf = new SimpleDateFormat();
		    tl.set(sdf);
		}
		sdf.applyPattern(format);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

		return sdf;
	}
	
	@Override
	public String convertDateToString(Long date, String format) {
	
		SimpleDateFormat sdf = getFormatter(format);
		return sdf.format(new Date(date));
	}

	@Override
	public Long convertStringToDate(String date, String format) {
		return null;
	}

}
