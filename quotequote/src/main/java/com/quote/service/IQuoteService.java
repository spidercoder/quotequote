package com.quote.service;


import com.quote.dto.Quote;
import com.quote.dto.request.QuoteByOwnerRequest;
import com.quote.dto.request.QuoteByTagRequest;
import com.quote.dto.request.QuoteRequest;
import com.quote.dto.response.QuoteResponse;

public interface IQuoteService {
	
	public int addQuote(QuoteRequest request, Long ownerId);
	
	public int updateQuote(QuoteRequest request);
	
	public QuoteResponse getQuotesByOwner(QuoteByOwnerRequest request);

	public int getQuoteCountByOwner(Long ownerId);
	
	public int getQuoteCountByTag(Long ownerId, String tag);
	
	public QuoteResponse getQuotesByTag(QuoteByTagRequest request);
	
	public Quote getQuoteById(Long id);
	
}
