var hostname = '/v1';

// Singleton class is sufficient for now, be careful about 'this'
var restApi = {
	getRequest: function(url) {
		return new Promise(function(fulfill, reject) {
			$.ajax({
				url: url,
				dataType: 'json',
				async: false,
				success: function (result) {
					console.log(url+"\n", result);
					fulfill(result);
				},
				error: function(xhr, status, text) {
					console.error(text);
					reject(new Error(text));
				}
			});
		});
	},
	
	postRequest: function(url, name, data) {
		var _this = this;
		var newData = {};
		//newData[name] = JSON.stringify(data);
		//console.log(JSON.stringify(data));
		return _this.postRequestJson(url, JSON.stringify(data));
	},
	
	postRequestJson: function(url, data) {
		return new Promise(function(fulfill, reject) {
			$.ajax({
				url: url,
				type: "POST",
				data: data,
				async: true,
				contentType: "application/json",
				success: function (result) {
					console.log(url+"\n", result);
					fulfill(result);
				},
				error: function(xhr, status, text) {
					console.error(xhr.responseText);
					reject(new Error(xhr.responseText));
				}
			});
		});
	},
	
	putRequestJson: function(url, data) {
		return new Promise(function(fulfill, reject) {
			$.ajax({
				url: url,
				type: "PUT",
				data: data,
				async: true,
				success: function (result) {
					console.log(url+"\n", result);
					fulfill(result);
				},
				error: function(xhr, status, text) {
					console.error(xhr.responseText);
					reject(new Error(xhr.responseText));
				}
			});
		});
	},
	
	deleteRequest: function(url) {
		return new Promise(function(fulfill, reject) {
			$.ajax({
				url: url,
				type: "delete",
				async: true,
				success: function (result) {
					fulfill(result);
				},
				error: function(xhr, status, text) {
					console.error(e.message || e);
					reject(new Error(e.message || e));
				}
			});
		});
	},
	
	prepareQuote: function(quote) {
		// Define the default quote
		var defaultQuote = {
			"quoteId":'',  // Can not add the default quote because quoteId is empty
			"name":'',
			"sTags":'',
			"quoteText":'',
			"url":'',
			"description":'',
			"attributedTo":'',
			"interest":5,
			"image":'',
			"creationDate":Date.now().getTime(),
			"lastModifiedDate":Date.now().getTime(),
			"lastViewedDate":Date.now().getTime(),
			"timesViewed":1,
			"mood":'',
			'copiedFrom':{},
			"isPrivate":false,
		};
		
		// Merge the objects, do not remove anything from quote yet
		quote = $.extend({},defaultQuote,quote);
		
		// Correctly handle the quoteId. Make sure it's a number.
		if(quote.quoteId != undefined ) {
			if(quote.quoteId != '') {
				quote.quoteId = parseInt(quote.quoteId);
			} else {
				delete quote.quoteId;
			}
		}
		
		if(quote.interest === '' || isNaN(quote.interest)) quote.interest = 5;
		
		if(quote.creationDate) quote.creationDate = parseInt(quote.creationDate);
		if(isNaN(quote.creationDate) || quote.creationDate == "") quote.creationDate = Date.now().getTime();
		
		// Make sure the boolean values are really booleans. If coming from a serialized form, they
		// might be the string 'on'
		if( quote.isPrivate ) quote.isPrivate = true;   else quote.isPrivate = false;
		
		// Remove extra properties (properties in quote that aren't in defaultQuote)
		for( var property in quote ) {
			if( quote.hasOwnProperty(property) ) {
				if( !defaultQuote.hasOwnProperty(property) ) {
					delete quote[property];
				}
			}
		}
		
		return quote;
	},
	
	putQuote: function(quote) {
		console.log("Put Quote");
		quote = this.prepareQuote(quote);
		console.log(quote);
		quote.lastModifiedDate = Date.now().getTime();
		
		var _this = this;
		return new Promise(function(fulfill, reject) {
			if(quote.quoteId != null) quote.quoteId = parseInt(quote.quoteId);
			_this.postRequest(hostname+'/quotes/createQuote','quote',quote).then(
				function(newQuote) {
					console.log('Quote Saved.');
					//fulfill(JSON.parse(newQuote)['message']);
				}, function(e) {
					console.error(e.message || e);
					reject(new Error(e.message || e));
				}
			);
		});
	},
}

// Get Request Parameter
// This is a pretty inefficient implementation and only meant for one time use
function getRequestParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	var results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function showMessage(message,title) {
	if(title == undefined) title = "Error";
	$('#messageModalTitle').html(title);
	$('#errorMessage').empty();
	$('#errorMessage').text(message);
	$('#messageModal').modal({show:true});
}