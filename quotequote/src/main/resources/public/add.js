data = {};

function saveForm(formName) {
	var array = jQuery(formName).serializeArray();
	
	// Because serializeArray() ignores unset checkboxes and radio buttons:
	array = array.concat(
		jQuery(formName+' input[type=checkbox]:not(:checked)').map(
			function() { return {"name": this.name, "value": false} }).get()
		);
	
	// Convert to a javascript object
	var json = {};
	jQuery.each(array, function() {
		json[this.name] = this.value || '';
	});
	json.quoteId = '';
	
	// Save Quote
	restApi.putQuote(json).then(function(quote) {
		window.close();
	}, function(error) {
		showMessage(error);
	});
}

$(document).ready(function(){
	$("#editModalSave").click(function() { saveForm("#main-quote-form"); });
	$("#editModalClose").click(function() { window.close() });

	$('#main-quote-form input[name=name]').val(decodeURIComponent(getRequestParameterByName("title")));
	$('#main-quote-form input[name=url]').val(decodeURIComponent(getRequestParameterByName("url")));
	$('#main-quote-form textarea[name=quoteText]').val(decodeURIComponent(getRequestParameterByName("quote")));

	if( getRequestParameterByName("settags") == "yes" ) {

	} else if( getRequestParameterByName("noui") == "yes" ) {
		$("#editModalSave").click();
	}
});