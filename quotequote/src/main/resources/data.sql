
insert into quote (	owner_user_id, name, s_tags,quote_text) values (1, 'test', 'computer', 'Start by doing whats necessary; then do whats possible; and suddenly you are doing the impossible.
Read more at: http://www.brainyquote.com/quotes/quotes/f/francisofa121023.html?src=t_inspirational' );
insert into quote (	owner_user_id, name, s_tags, quote_text, attributed_to) values (1, 'test1', 'physics', 'Perfection is not attainable, but if we chase perfection we can catch excellence.
Read more at: http://www.brainyquote.com/quotes/quotes/v/vincelomba385070.html?src=t_inspirational', 'Vince lombardi');
insert into quote (	owner_user_id, name, s_tags, quote_text, attributed_to) values (1, 'tes2', 'comedy', 'There is nothing impossible to him who will try.
Read more at: http://www.brainyquote.com/quotes/quotes/a/alexandert148611.html?src=t_inspirational', 'Alexander the great');
insert into quote (	owner_user_id, name, s_tags, quote_text, attributed_to) values (1, 'tes3', 'computer', 'We know what we are, but know not what we may be.
Read more at: http://www.brainyquote.com/quotes/quotes/w/williamsha164317.html?src=t_inspirational', 'William Shakespeare');
insert into quote (	owner_user_id, name, s_tags, quote_text, attributed_to) values (1, 'tes4', 'computer', 'Try to be a rainbow in someones cloud.
Read more at: http://www.brainyquote.com/quotes/quotes/m/mayaangelo578763.html?src=t_inspirational', 'Anonymous');
insert into quote (	owner_user_id, name, s_tags, quote_text, attributed_to) values (1, 'tes5', 'cooking', 'Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do. If you havent found it yet, keep looking. Dont settle. As with all matters of the heart, youll know when you find it.
Read more at: http://www.brainyquote.com/quotes/quotes/s/stevejobs416859.html?src=t_inspirational', 'steve jobs');